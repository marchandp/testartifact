.PHONY: clean pythran

all: C password

C:
	  gcc helloworld.c -o helloworld.e

password:
	  echo "coucou" 

clean:
	  rm -rf *.o *.e *.so *.pyf *.pyc __pycache__


PROJECT_NAME := $(CI_PROJECT_NAME)
REGISTRY_PASSWORD := $(CI_REGISTRY_PASSWORD)
REGISTRY_USER := $(CI_REGISTRY_USER)
REGISTRY := $(CI_REGISTRY)
PKG := "gitlab.com/marchandp/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
PASSFILEPATH = $(MYPASSFILEPATH)
